package com.example.wsnetty.handlers;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.example.wsnetty.MemoryAppender;
import io.netty.channel.Channel;
import org.apache.logging.log4j.util.Strings;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.netty.DisposableServer;
import reactor.netty.NettyOutbound;
import reactor.netty.NettyPipeline;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.http.websocket.WebsocketOutbound;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

class WriteTimeoutHandlerTest {

    private MemoryAppender memoryAppender;

    private DisposableServer server;
    private Channel channel;

    private Thread clientThread;

    @BeforeEach
    void setUp() {
        initInMemoryLogger();

        server = HttpServer.create()
                .port(8095)
                .doOnChannelInit((connectionObserver, channel, remoteAddress) -> {
                    this.channel = channel;
                    channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, WriteTimeoutHandler.NAME, new WriteTimeoutHandler(10, TimeUnit.SECONDS));
                })
                .route(routes -> routes.ws("/test", this::subscribe))
                .bindNow();
    }

    @AfterEach
    void tearDown() {
        server.disposeNow();
        clientThread.interrupt();
    }

    @Test
    void timeoutTest() {
        clientThread = new Thread(this::initClient);
        clientThread.setDaemon(true);
        clientThread.start();

        System.out.println("Test started");
        Awaitility.await()
                .atMost(Duration.ofSeconds(60))
                .pollInterval(Duration.ofSeconds(1))
                .until(() -> !channel.isActive());

        System.out.println("Await finished");

        Assertions.assertFalse(channel.isActive());
        Assertions.assertFalse(channel.isOpen());
        Assertions.assertFalse(channel.isRegistered());

        Assertions.assertTrue(memoryAppender.contains("Too slow client. Disconnecting...", Level.INFO));
    }

    private void initInMemoryLogger() {
        Logger logger = (Logger) LoggerFactory.getLogger(WriteTimeoutHandler.class);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        logger.setLevel(Level.DEBUG);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    private void initClient() {
        HttpClient client = HttpClient.create();
        client.websocket()
                .uri("ws://localhost:8095/test")
                .handle((inbound, outbound) -> {
                    inbound.receive()
                            .asString()
                            .subscribe(s -> {
                                try {
                                    TimeUnit.SECONDS.sleep(3);
                                } catch (InterruptedException e) {
                                    Thread.currentThread().interrupt();
                                }
                                System.out.println("Consumed " + s.substring(0, 6));
                            });
                    return outbound.neverComplete();
                })
                .blockLast();
    }

    private NettyOutbound subscribe(WebsocketInbound in, WebsocketOutbound out) {
        AtomicLong atomicLong = new AtomicLong(0);
        return out.sendString(Flux.generate((sink) -> {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sink.next(atomicLong.incrementAndGet());
        })
                .doOnNext(s -> System.out.println("Generated " + s))
                .map(n -> Strings.repeat(String.valueOf(n), 10000)));
    }
}