package com.example.wsnetty.handlers;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.example.wsnetty.MemoryAppender;
import io.netty.channel.Channel;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionTimeoutException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import reactor.netty.DisposableServer;
import reactor.netty.NettyPipeline;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.server.HttpServer;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

class IdleStateHandlerTest {

    private MemoryAppender memoryAppender;

    private DisposableServer server;
    private Channel channel;

    @BeforeEach
    void setUp() {
        initInMemoryLogger();
    }

    @AfterEach
    void tearDown() {
        server.disposeNow();
    }

    @Test
    void testWithoutHandler() {
        startNettyServer(null);

        Thread clientThread = new Thread(this::initClient);
        clientThread.setDaemon(true);
        clientThread.start();

        System.out.println("Test started");

        Assertions.assertThrows(ConditionTimeoutException.class, () -> Awaitility.await()
                    .atMost(Duration.ofSeconds(10))
                    .pollInterval(Duration.ofSeconds(1))
                    .until(() -> !channel.isActive()));

        Assertions.assertTrue(channel.isActive());
        Assertions.assertTrue(channel.isOpen());
        Assertions.assertTrue(channel.isRegistered());
    }

    @Test
    void testWithHandlerWithoutLimits() {
        startNettyServer(new IdleStateHandler(0, 0, 0));

        Thread clientThread = new Thread(this::initClient);
        clientThread.setDaemon(true);
        clientThread.start();

        System.out.println("Test started");

        Assertions.assertThrows(ConditionTimeoutException.class, () -> Awaitility.await()
                .atMost(Duration.ofSeconds(10))
                .pollInterval(Duration.ofSeconds(1))
                .until(() -> !channel.isActive()));

        Assertions.assertTrue(channel.isActive());
        Assertions.assertTrue(channel.isOpen());
        Assertions.assertTrue(channel.isRegistered());
    }

    @Test
    void testReadIdleState() {
        startNettyServer(new IdleStateHandler(5, 0, 0, TimeUnit.SECONDS));

        startClientAndAssertResult();
    }

    @Test
    void testWriteIdleState() {
        startNettyServer(new IdleStateHandler(0, 5, 0, TimeUnit.SECONDS));

        startClientAndAssertResult();
    }

    @Test
    void testAllIdleState() {
        startNettyServer(new IdleStateHandler(0, 0, 5, TimeUnit.SECONDS));

        startClientAndAssertResult();
    }

    private void startNettyServer(IdleStateHandler handler) {
        server = HttpServer.create()
                .port(8095)
                .doOnChannelInit((connectionObserver, channel1, remoteAddress) -> {
                    this.channel = channel1;
                    if (handler != null) {
                        channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, IdleStateHandler.NAME, handler);
                    }
                })
                .route(routes -> routes.ws("/test", (websocketInbound, websocketOutbound) -> websocketOutbound.neverComplete()))
                .bindNow();
    }

    private void startClientAndAssertResult() {
        Thread clientThread = new Thread(this::initClient);
        clientThread.setDaemon(true);
        clientThread.start();

        System.out.println("Test started");
        Awaitility.await()
                .atMost(Duration.ofSeconds(10))
                .pollInterval(Duration.ofSeconds(1))
                .until(() -> !channel.isActive());

        System.out.println("Await finished");

        Assertions.assertFalse(channel.isActive());
        Assertions.assertFalse(channel.isOpen());
        Assertions.assertFalse(channel.isRegistered());

        Assertions.assertTrue(memoryAppender.contains("Client is idle. Disconnecting...", Level.INFO));
    }

    private void initInMemoryLogger() {
        Logger logger = (Logger) LoggerFactory.getLogger(IdleStateHandler.class);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        logger.setLevel(Level.DEBUG);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    private void initClient() {
        HttpClient client = HttpClient.create();
        client.websocket()
                .uri("ws://localhost:8095/test")
                .handle((inbound, outbound) -> {
                    System.out.println("Client handle");
                    inbound.receive()
                            .asString()
                            .subscribe(s -> {
                                System.out.println("Msg is " + s);
                            });
                    return outbound.neverComplete();
                })
                .blockLast();
    }
}