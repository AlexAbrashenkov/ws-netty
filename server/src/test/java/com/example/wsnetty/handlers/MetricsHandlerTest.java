package com.example.wsnetty.handlers;

import com.example.wsnetty.server.WsSubscribersHolder;
import io.netty.channel.Channel;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.NettyPipeline;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientResponse;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MetricsHandler.class, WsSubscribersHolder.class})
class MetricsHandlerTest {

    private DisposableServer server;
    private Channel channel;

    @Autowired private MetricsHandler metricsHandler;
    @Autowired private WsSubscribersHolder subscribersHolder;

    @BeforeEach
    void setUp() {
        startNettyServer();
        metricsHandler.getMetricsCollector().clear();
    }

    @AfterEach
    void tearDown() {
        server.disposeNow();
    }

    @Test
    void measureHttpRequestTime() {
        HttpClientResponse resp = HttpClient.create()
                .get()
                .uri("http://localhost:8095/http")
                .response()
                .block();

        Assertions.assertEquals(200, resp.status().code());

        org.assertj.core.api.Assertions.assertThat(metricsHandler.getMetricsCollector().getMetrics(MetricsHandler.MetricsCollector.RequestType.HTTP))
                .hasSize(1)
                .allMatch(reqTime -> reqTime >= TimeUnit.MILLISECONDS.toNanos(300));
    }

    @Test
    void measureWebSocketTime() {
        HttpClient client = HttpClient.create();
        client.websocket()
                .uri("ws://localhost:8095/topic/test")
                .handle((inbound, outbound) -> {
                    System.out.println("Client handle");
                    return inbound.receive()
                            .asString()
                            .take(5)
                            .doOnNext(s -> System.out.println("Received msg " + s));
                })
                .blockLast();

        Awaitility.await()
                .atMost(Duration.ofSeconds(1))
                .until(() -> !channel.isOpen());

        org.assertj.core.api.Assertions.assertThat(metricsHandler.getMetricsCollector().getMetrics(MetricsHandler.MetricsCollector.RequestType.WS))
                .hasSize(6)
                .allMatch(writeTime -> writeTime > 0);
    }

    private void startNettyServer() {
        server = HttpServer.create()
                .port(8095)
                .doOnChannelInit((connectionObserver, channel1, remoteAddress) -> {
                    this.channel = channel1;
                    channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, WSChannelHandler3.NAME, new WSChannelHandler3(subscribersHolder));
                    channel.pipeline().addBefore(WSChannelHandler3.NAME, MetricsHandler.NAME, metricsHandler);
                })
                .route(routes -> routes
                        .ws("/test", (inbound, outbound) -> outbound.neverComplete())
                        .get("/http", this::handleHttpRequest)
                )
                .bindNow();
    }

    private Publisher<Void> handleHttpRequest(HttpServerRequest req, HttpServerResponse resp) {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        return resp.sendString(Mono.just("Request was received"));
    }
}