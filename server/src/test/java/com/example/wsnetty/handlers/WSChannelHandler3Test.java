package com.example.wsnetty.handlers;

import com.example.wsnetty.entity.PairEntity;
import com.example.wsnetty.server.WsSubscribersHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.*;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.NettyPipeline;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientResponse;
import reactor.netty.http.server.HttpServer;

import java.time.Duration;
import java.util.List;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MetricsHandler.class, WsSubscribersHolder.class})
class WSChannelHandler3Test {

    private DisposableServer server;
    private Channel channel;
    private List<String> pipelineHandlersNames;

    @Autowired private MetricsHandler metricsHandler;
    @Autowired private WsSubscribersHolder subscribersHolder;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        startNettyServer();
        metricsHandler.getMetricsCollector().clear();
    }

    @AfterEach
    void tearDown() {
        server.disposeNow();
    }

    @Test
    void handleHttpRequest() {
        HttpClientResponse resp = HttpClient.create()
                .get()
                .uri("http://localhost:8095/http")
                .response()
                .block();

        Assertions.assertEquals(200, resp.status().code());

        org.assertj.core.api.Assertions.assertThat(pipelineHandlersNames)
                .hasSizeGreaterThanOrEqualTo(6)
                .containsSequence(NettyPipeline.HttpCodec,
                        NettyPipeline.HttpTrafficHandler,
                        MetricsHandler.NAME,
                        WSChannelHandler3.NAME,
                        "testPipelineHandler",
                        NettyPipeline.ReactiveBridge);
    }

    @Test
    void handleWebSocketMsg() {
        HttpClient client = HttpClient.create();
        client.websocket()
                .uri("ws://localhost:8095/topic/test")
                .handle((inbound, outbound) -> {
                    System.out.println("Client handle");
                    return inbound.receive()
                            .asString()
                            .take(2)
                            .doOnNext(s -> {
                                try {
                                    PairEntity pair = objectMapper.readValue(s, PairEntity.class);
                                    Assertions.assertEquals("Generated value " + pair.id(), pair.name());
                                } catch (JsonProcessingException e) {
                                    throw new RuntimeException("Wrong type of received object", e);
                                }
                            });
                })
                .blockLast();

        Awaitility.await()
                .atMost(Duration.ofSeconds(1))
                .until(() -> !channel.isOpen());

        org.assertj.core.api.Assertions.assertThat(pipelineHandlersNames)
                .hasSizeGreaterThanOrEqualTo(7)
                .containsSequence("wsencoder",
                        "wsdecoder",
                        MetricsHandler.NAME,
                        WriteTimeoutHandler.NAME,
                        IdleStateHandler.NAME,
                        WSChannelHandler3.NAME,
                        "testPipelineHandler");
    }

    private void startNettyServer() {
        server = HttpServer.create()
                .port(8095)
                .doOnChannelInit((connectionObserver, channel1, remoteAddress) -> {
                    this.channel = channel1;
                    channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, WSChannelHandler3.NAME, new WSChannelHandler3(subscribersHolder));
                    channel.pipeline().addBefore(WSChannelHandler3.NAME, MetricsHandler.NAME, metricsHandler);
                    channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, "testPipelineHandler", new ChannelInboundHandlerAdapter() {
                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                            pipelineHandlersNames = ctx.pipeline().names();
                            super.channelRead(ctx, msg);
                        }
                    });
                })
                .route(routes -> routes
                        .ws("/test", (inbound, outbound) -> outbound.neverComplete())
                        .get("/http", (req, resp) -> resp.sendString(Mono.just("Request was received")))
                )
                .bindNow();
    }
}