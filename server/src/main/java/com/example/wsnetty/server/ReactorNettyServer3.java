package com.example.wsnetty.server;

import com.example.wsnetty.handlers.MetricsHandler;
import com.example.wsnetty.handlers.WSChannelHandler3;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.NettyPipeline;
import reactor.netty.http.server.HttpServer;

import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class ReactorNettyServer3 {

    private static final int PORT = 8099;

    private final WsSubscribersHolder channelsHolder;
    private final MetricsHandler metricsHandler;

    @Bean
    public DisposableServer httpServer() {
        HttpServer httpServer = HttpServer.create()
                .host("0.0.0.0")
                .port(PORT)
                .doOnChannelInit((connectionObserver, channel, remoteAddress) -> {
                    channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, WSChannelHandler3.NAME, new WSChannelHandler3(channelsHolder));
                    channel.pipeline().addBefore(WSChannelHandler3.NAME, MetricsHandler.NAME, metricsHandler);
                })
                .route(routes -> routes.get("/path/{param}", (req, resp) -> resp.sendString(Mono.just(req.param("param")))));

        DisposableServer disposableServer = httpServer.bindNow();
        log.info("Netty started on port {}", PORT);
        return disposableServer;
    }
}
