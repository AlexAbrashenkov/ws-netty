package com.example.wsnetty.server;

import com.example.wsnetty.entity.PairEntity;
import com.example.wsnetty.handlers.WSChannelHandler3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Slf4j
public class WsSubscribersHolder {

    private final Map<String, Set<WSChannelHandler3>> subscriptions = new HashMap<>();

    @PostConstruct
    public void setUp() {
        String path = "/topic/test";
        Flux.interval(Duration.ofSeconds(1))
                .doOnNext(aLong -> log.info("num {}", aLong))
                .subscribe(num -> {
                    Set<WSChannelHandler3> handlers = subscriptions.get(path);
                    if (!CollectionUtils.isEmpty(handlers)) {
                        Set<WSChannelHandler3> handlersCopy = new HashSet<>(handlers);
                        handlersCopy.forEach(handler -> handler.sendMessage(new PairEntity(num, "Generated value " + num, System.nanoTime())));
                    }
                });
    }

    public void subscribe(String path, WSChannelHandler3 handler) {
        Set<WSChannelHandler3> handlers = subscriptions.getOrDefault(path, new HashSet<>());
        handlers.add(handler);
        subscriptions.put(path, handlers);
    }

    public void unsubscribe(String path, WSChannelHandler3 pipeline) {
        Set<WSChannelHandler3> handlers = subscriptions.get(path);
        handlers.remove(pipeline);
        subscriptions.put(path, handlers);
    }
}
