package com.example.wsnetty.client;

import lombok.extern.slf4j.Slf4j;
import reactor.netty.NettyPipeline;
import reactor.netty.http.client.HttpClient;

@Slf4j
public class TestWebSocketClient {

    public static void main(String[] args) {
        HttpClient client = HttpClient.create();

        client
                .doOnChannelInit((connectionObserver, channel, remoteAddress) ->
                        channel.pipeline().addBefore(NettyPipeline.ReactiveBridge, "handler", new ClientWebSocketHandler()))
                .compress(true)
                .websocket()
                .uri("ws://localhost:8099/topic/test")
                .handle((websocketInbound, websocketOutbound) -> {
                    websocketInbound.receive()
                            .asString()
                            .subscribe(s -> {
                                log.info("Msg is {}", s);
                            });
                    return websocketOutbound.neverComplete();
                })
                .blockLast();
    }
}
