package com.example.wsnetty.entity;

public record WrappedNettyFrame(Object msg, Long receivedTime) {

}
