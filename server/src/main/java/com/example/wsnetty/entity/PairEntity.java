package com.example.wsnetty.entity;

public record PairEntity(long id, String name, long generationTime) {}