package com.example.wsnetty.archived.by_channel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import lombok.RequiredArgsConstructor;
import reactor.netty.NettyPipeline;

@RequiredArgsConstructor
public class WSChannelHandler extends ChannelInboundHandlerAdapter {

    private final ChannelsHolder channelsHolder;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest httpRequest) {
            HttpHeaders headers = httpRequest.headers();
            if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION))
                && "websocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                ChannelPipeline pipeline = ctx.pipeline();
                pipeline.remove(NettyPipeline.ReactiveBridge);
                if (pipeline.get(NettyPipeline.HttpTrafficHandler) != null) {
                    pipeline.remove(NettyPipeline.HttpTrafficHandler);
                }
                if (pipeline.get(NettyPipeline.AccessLogHandler) != null) {
                    ctx.pipeline().remove(NettyPipeline.AccessLogHandler);
                }
                if (pipeline.get(NettyPipeline.HttpMetricsHandler) != null) {
                    ctx.pipeline().remove(NettyPipeline.HttpMetricsHandler);
                }

                handleHandshake(ctx, httpRequest);
                System.out.println("handshake is done. Handlers: " + ctx.pipeline().toMap());

                channelsHolder.subscribe(httpRequest.uri(), pipeline);
            }
        }

        ctx.pipeline().remove(this);
        ctx.fireChannelRead(msg);
    }

    private void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            HttpRequest fullRequest = new DefaultFullHttpRequest(req.protocolVersion(), req.method(), req.uri());
            fullRequest.headers().set(req.headers());
            handshaker.handshake(ctx.channel(), fullRequest);
        }
    }

    private String getWebSocketURL(HttpRequest request) {
        return  "ws://" + request.headers().get(HttpHeaderNames.HOST) + request.uri();
    }
}
