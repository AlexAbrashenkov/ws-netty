package com.example.wsnetty.archived.by_channel;

import com.example.wsnetty.entity.PairEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//@Component
@Slf4j
public class ChannelsHolder {

    private final Map<String, Set<ChannelPipeline>> subscriptions = new HashMap<>();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    public void setUp() {
        System.out.println("initializing ChannelHolder");
        String path = "/topic/test";
        Flux.interval(Duration.ofSeconds(3))
                .doOnNext(aLong -> log.info("num {}", aLong))
                .subscribe(s -> {
                    Set<ChannelPipeline> channelPipelines = subscriptions.get(path);
                    if (!CollectionUtils.isEmpty(channelPipelines)) {
                        Set<ChannelPipeline> pipelines = new HashSet<>(channelPipelines);
                        for (ChannelPipeline pipeline : pipelines) {
                            if (pipeline.channel().isActive()) {
                                Map<String, ChannelHandler> stringChannelHandlerMap = pipeline.toMap();
                                System.out.println("Sending message. Handlers in pipeline: " + stringChannelHandlerMap);

                                try {
                                    pipeline.channel().writeAndFlush(new TextWebSocketFrame(objectMapper.writeValueAsString(new PairEntity(s, "Generated value " + s, System.nanoTime()))));
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                pipeline.channel().close();
                                unsubscribe(path, pipeline);
                            }
                        }
                    }
                });
    }

    public void subscribe(String path, ChannelPipeline pipeline) {
        Set<ChannelPipeline> channels = subscriptions.getOrDefault(path, new HashSet<>());
        channels.add(pipeline);
        subscriptions.put(path, channels);
    }

    private void unsubscribe(String path, ChannelPipeline pipeline) {
        Set<ChannelPipeline> channels = subscriptions.get(path);
        channels.remove(pipeline);
        subscriptions.put(path, channels);
    }
}
