package com.example.wsnetty.archived.by_handler;

import com.example.wsnetty.entity.PairEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import reactor.netty.NettyPipeline;

@RequiredArgsConstructor
public class WSChannelHandler2 extends ChannelInboundHandlerAdapter {

    private final ChannelsHolder2 channelsHolder;
    private ChannelHandlerContext ctx;
    private String path;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest httpRequest) {
            HttpHeaders headers = httpRequest.headers();
            if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION))
                    && "websocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                removeNonWSHandlers(ctx);
                handleHandshake(ctx, httpRequest);
                System.out.println("handshake is done. Handlers: " + ctx.pipeline().toMap());

                String path = httpRequest.uri();
                this.path = path;
                channelsHolder.subscribe(path, this);
            }
        }

        ctx.fireChannelRead(msg);
    }

    public void sendMessage(String msg) {
        if (ctx.channel().isActive()) {
            System.out.println("sending message through handlers " + ctx.pipeline().toMap());
            ctx.writeAndFlush(new TextWebSocketFrame(msg));
        } else {
            ctx.close();
            channelsHolder.unsubscribe(path, this);
        }
    }

    @SneakyThrows
    public void sendMessage(PairEntity pairEntity) {
        if (ctx.channel().isActive()) {
            System.out.println("Sending msg with handlers " + ctx.pipeline().toMap());
            ctx.writeAndFlush(new TextWebSocketFrame(objectMapper.writeValueAsString(pairEntity)));
        }
    }

    private void removeNonWSHandlers(ChannelHandlerContext ctx) {
        ChannelPipeline pipeline = ctx.pipeline();
        pipeline.remove(NettyPipeline.ReactiveBridge);
        if (pipeline.get(NettyPipeline.HttpTrafficHandler) != null) {
            pipeline.remove(NettyPipeline.HttpTrafficHandler);
        }
        if (pipeline.get(NettyPipeline.AccessLogHandler) != null) {
            ctx.pipeline().remove(NettyPipeline.AccessLogHandler);
        }
        if (pipeline.get(NettyPipeline.HttpMetricsHandler) != null) {
            ctx.pipeline().remove(NettyPipeline.HttpMetricsHandler);
        }
    }

    private void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            HttpRequest fullRequest = new DefaultFullHttpRequest(req.protocolVersion(), req.method(), req.uri());
            fullRequest.headers().set(req.headers());
            handshaker.handshake(ctx.channel(), fullRequest);
        }
    }

    private String getWebSocketURL(HttpRequest request) {
        return "ws://" + request.headers().get(HttpHeaderNames.HOST) + request.uri();
    }
}
