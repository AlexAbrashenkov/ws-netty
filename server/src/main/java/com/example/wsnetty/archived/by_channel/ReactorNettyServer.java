package com.example.wsnetty.archived.by_channel;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.server.WebsocketServerSpec;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.http.websocket.WebsocketOutbound;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

//@Configuration
@Slf4j
@RequiredArgsConstructor
public class ReactorNettyServer {

    HttpServer httpServer;
    private final ChannelsHolder channelsHolder;

    @Bean
    public DisposableServer httpServer() {
        int port = 8099;
        httpServer = HttpServer.create()
                .host("localhost")
                .port(port)
                .doOnConnection(connection -> {
//                    connection.channel().pipeline().addBefore(NettyPipeline.ReactiveBridge, "wsHandler", new WSChannelHandler(channelsHolder));
                    System.out.println("onConnection: " + connection.channel().pipeline());
                })
                .route(routes -> routes
                        .ws("/**", this::subscribe, WebsocketServerSpec.builder().compress(true).build())
                        .get("/path/{param}", (req, resp) -> resp.sendString(Mono.just(req.param("param")))));

        DisposableServer disposableServer = httpServer.bindNow();
        log.info("Netty started on port {}", port);
        return disposableServer;
    }

    @SneakyThrows
    private Publisher<Void> subscribe(WebsocketInbound inbound, WebsocketOutbound outbound) {
        Field pathField = inbound.getClass().getSuperclass().getDeclaredField("path");
        pathField.setAccessible(true);
        String path = (String) ReflectionUtils.getField(pathField, inbound);

        Method channelMethod = inbound.getClass().getMethod("channel");
        SocketChannel channel = (SocketChannel) ReflectionUtils.invokeMethod(channelMethod, inbound);
        ChannelPipeline pipeline = channel.pipeline();

        channelsHolder.subscribe(path, pipeline);

        return outbound.send(inbound.receive().retain());
    }
}
