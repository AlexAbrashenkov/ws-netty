package com.example.wsnetty.archived.by_handler;

import com.example.wsnetty.entity.PairEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//@Component
@Slf4j
public class ChannelsHolder2 {

    private final Map<String, Set<WSChannelHandler2>> subscriptions = new HashMap<>();

    @PostConstruct
    public void setUp() {
        String path = "/topic/test";
        Flux.interval(Duration.ofSeconds(3))
                .doOnNext(aLong -> log.info("num {}", aLong))
                .subscribe(num -> {
                    Set<WSChannelHandler2> handlers = subscriptions.get(path);
                    if (!CollectionUtils.isEmpty(handlers)) {
                        Set<WSChannelHandler2> handlersCopy = new HashSet<>(handlers);
                        handlersCopy.forEach(handler -> handler.sendMessage(new PairEntity(num, "Generated value " + num, System.nanoTime())));
                    }
                });
    }

    public void subscribe(String path, WSChannelHandler2 handler) {
        Set<WSChannelHandler2> handlers = subscriptions.getOrDefault(path, new HashSet<>());
        handlers.add(handler);
        subscriptions.put(path, handlers);
    }

    public void unsubscribe(String path, WSChannelHandler2 pipeline) {
        Set<WSChannelHandler2> handlers = subscriptions.get(path);
        handlers.remove(pipeline);
        subscriptions.put(path, handlers);
    }
}
