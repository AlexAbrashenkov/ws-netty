package com.example.wsnetty.archived.by_handler;

import io.netty.channel.ChannelPipeline;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

//@Configuration
@Slf4j
@RequiredArgsConstructor
public class ReactorNettyServer2 {

    private static final int PORT = 8099;

    private final ChannelsHolder2 channelsHolder;

    @Bean
    public DisposableServer httpServer() {
        HttpServer httpServer = HttpServer.create()
                .host("localhost")
                .port(PORT)
                .doOnConnection(connection -> {
                    ChannelPipeline pipeline = connection.channel().pipeline();
//                    if (pipeline.get("wsHandler") == null) {
//                        pipeline.addBefore(NettyPipeline.ReactiveBridge, "wsHandler", new WSChannelHandler2(channelsHolder));
//                    }
                    System.out.println("onConnection: " + pipeline);
                })
                .route(routes -> routes
                        .get("/path/{param}", (req, resp) -> resp.sendString(Mono.just(req.param("param")))));

        DisposableServer disposableServer = httpServer.bindNow();
        log.info("Netty started on port {}", PORT);
        return disposableServer;
    }
}
