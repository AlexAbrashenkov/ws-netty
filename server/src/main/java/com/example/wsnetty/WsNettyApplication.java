package com.example.wsnetty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsNettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsNettyApplication.class, args);
    }

}
