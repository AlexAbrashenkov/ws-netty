package com.example.wsnetty.handlers;

import io.netty.channel.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
public class WriteTimeoutHandler extends ChannelOutboundHandlerAdapter {
    public static final String NAME = "writeTimeoutHandler";
    private static final long DEFAULT_TIMEOUT = TimeUnit.SECONDS.toNanos(30);

    private final long writeTimeout;
    private boolean closed;

    private Future<?> timeoutTaskFuture;
    private final Queue<Long> timestampsQueue = new ConcurrentLinkedQueue<>();

    public WriteTimeoutHandler(long writeTimeout, TimeUnit timeUnit) {
        if (writeTimeout <= 0) {
            this.writeTimeout = DEFAULT_TIMEOUT;
        } else {
            this.writeTimeout = timeUnit.toNanos(writeTimeout);
        }
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        stopScheduledTask();
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) {
        if (!ctx.channel().isWritable()) {
            promise = promise.unvoid();
            addListener(ctx, promise);
            startScheduledTask(ctx);
        }
        ctx.write(msg, promise);
    }

    private void addListener(ChannelHandlerContext ctx, ChannelPromise promise) {
        timestampsQueue.offer(System.nanoTime());
        promise.addListener(f -> {
            Long startTimestamp = timestampsQueue.poll();
            checkExecutionTime(ctx, startTimestamp);
            if (timestampsQueue.isEmpty()) {
                stopScheduledTask();
            }
        });
    }

    private void checkExecutionTime(ChannelHandlerContext ctx, Long startTimestamp) {
        if (startTimestamp != null) {
            long executionTime = System.nanoTime() - startTimestamp;
            if (executionTime > writeTimeout) {
                writeTimedOut(ctx);
            }
        }
    }

    protected void writeTimedOut(ChannelHandlerContext ctx) {
        if (!closed) {
            ctx.close();
            log.info("Too slow client. Disconnecting...");
            closed = true;
            stopScheduledTask();
        }
    }

    private void startScheduledTask(ChannelHandlerContext ctx) {
        if (timeoutTaskFuture == null && ctx.channel().isOpen()) {
            Runnable task = () -> {
                Long earliestStartTimestamp = timestampsQueue.peek();
                checkExecutionTime(ctx, earliestStartTimestamp);
            };
            timeoutTaskFuture = ctx.executor().scheduleAtFixedRate(task, writeTimeout, writeTimeout / 3, TimeUnit.NANOSECONDS);
        }
    }

    private void stopScheduledTask() {
        if (timeoutTaskFuture != null && !timeoutTaskFuture.isDone()) {
            timeoutTaskFuture.cancel(false);
            timeoutTaskFuture = null;
        }
    }
}
