package com.example.wsnetty.handlers;

import com.example.wsnetty.entity.WrappedNettyFrame;
import io.netty.channel.*;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.wsnetty.handlers.MetricsHandler.MetricsCollector.RequestType.HTTP;
import static com.example.wsnetty.handlers.MetricsHandler.MetricsCollector.RequestType.WS;

@Slf4j
@Component
@ChannelHandler.Sharable
public class MetricsHandler extends ChannelDuplexHandler {

    public static final String NAME = "metricsHandler";

    long dataReceivedTime;
    @Getter private MetricsCollector metricsCollector = new MetricsCollector();

    @Override
    public void connect(ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise) throws Exception {
        log.info("Channel connected");
        super.connect(ctx, remoteAddress, localAddress, promise);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("Channel active");
        super.channelActive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        log.info("Processing request... {}", msg.getClass().getSimpleName());
        Object wrappedMsg = msg;
        if (msg instanceof HttpRequest) {
            dataReceivedTime = System.nanoTime();
        } else if (msg instanceof WebSocketFrame) {
            wrappedMsg = new WrappedNettyFrame(msg, System.nanoTime());
        }
        super.channelRead(ctx, wrappedMsg);
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        log.info("Responding... {}", msg.getClass().getSimpleName());
        Object unwrappedMsg = msg;
        if (msg instanceof WrappedNettyFrame wrapped) {
            unwrappedMsg = wrapped.msg();
            promise = promise.unvoid();
            promise.addListener(f -> {
                if (wrapped.receivedTime() != null) {
                    metricsCollector.calculate(WS, wrapped.receivedTime());
                }
            });
        } else if (msg instanceof LastHttpContent) {
            metricsCollector.calculate(HTTP, dataReceivedTime);
        }
        super.write(ctx, unwrappedMsg, promise);
    }


    static class MetricsCollector {

        enum RequestType {HTTP, WS}

        private final Map<RequestType, List<Long>> metrics;

        public MetricsCollector() {
            this.metrics = new HashMap<>();
            for (RequestType type : RequestType.values()) {
                this.metrics.put(type, new ArrayList<>());
            }
        }

        public void calculate(RequestType type, Long startTime) {
            this.metrics.get(type).add(System.nanoTime() - startTime);
        }

        public List<Long> getMetrics(RequestType type) {
            return metrics.get(type);
        }

        public void clear() {
            metrics.forEach((type, longs) -> longs.clear());
        }
    }
}
