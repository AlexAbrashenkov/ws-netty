package com.example.wsnetty.handlers;

import com.example.wsnetty.entity.PairEntity;
import com.example.wsnetty.entity.WrappedNettyFrame;
import com.example.wsnetty.server.WsSubscribersHolder;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import reactor.netty.NettyPipeline;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Slf4j
public class WSChannelHandler3 extends ChannelInboundHandlerAdapter {

    public static final String NAME = "wsHandler";
    private static final boolean ALLOW_WS_COMPRESSION = true;

    private final WsSubscribersHolder channelsHolder;
    private ChannelHandlerContext ctx;
    private String path;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Object unwrappedMsg = msg;
        Long receivedTime = null;
        if (msg instanceof WrappedNettyFrame wrappedFrame) {
            unwrappedMsg = wrappedFrame.msg();
            receivedTime = wrappedFrame.receivedTime();
        }

        if (unwrappedMsg instanceof HttpRequest httpRequest) {
            processHttpRequest(ctx, httpRequest, receivedTime);
            ctx.fireChannelRead(msg);
        } else if (unwrappedMsg instanceof WebSocketFrame frame) {
            processWebSocketFrame(ctx, frame, receivedTime);
            ctx.read();
        }
    }

    private void processHttpRequest(ChannelHandlerContext ctx, HttpRequest httpRequest, Long receivedTime) {
        HttpHeaders headers = httpRequest.headers();
        if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION))
                && "websocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

            removeNonWSHandlers(ctx);
            addCompressionHandler(ctx, httpRequest);
            addTimeoutHandlers(ctx);

            handleHandshake(ctx, httpRequest, receivedTime);
            log.info("handshake is done. Handlers: {}", ctx.pipeline().toMap());

            String path = httpRequest.uri();
            this.path = path;
            channelsHolder.subscribe(path, this);
        }
    }

    private void processWebSocketFrame(ChannelHandlerContext ctx, Object msg, Long receivedTime) {
        if (msg instanceof BinaryWebSocketFrame frame) {
            log.debug("BinaryWebSocketFrame received");
            ctx.channel().writeAndFlush(new WrappedNettyFrame(new BinaryWebSocketFrame(frame.content()), receivedTime));
        } else if (msg instanceof TextWebSocketFrame frame) {
            log.debug("TextWebSocketFrame received, text: {}", frame.text());
            ctx.channel().writeAndFlush(new WrappedNettyFrame(new TextWebSocketFrame("Message recieved : " + frame.text()), receivedTime));
        } else if (msg instanceof PingWebSocketFrame) {
            log.debug("PingWebSocketFrame received");
            ctx.channel().writeAndFlush(new WrappedNettyFrame(new PongWebSocketFrame(), receivedTime));
        } else if (msg instanceof PongWebSocketFrame) {
            log.debug("PondWebSocketFrame received");
        } else if (msg instanceof CloseWebSocketFrame frame) {
            log.info("CloseWebSocketFrame Received, reason: {}, statusCoode {}", frame.reasonText(), frame.statusCode());
            CloseWebSocketFrame closeFrame = new CloseWebSocketFrame(true, frame.rsv(), frame.content());
            ctx.channel().writeAndFlush(new WrappedNettyFrame(closeFrame, receivedTime));
        } else {
            log.warn("Unsupported WebSocketFrame with type {}", msg.getClass().getSimpleName());
        }
    }

    public void sendMessage(String msg) {
        if (ctx.channel().isActive()) {
            log.debug("sending message through handlers {}", ctx.pipeline().toMap());
            ctx.writeAndFlush(new TextWebSocketFrame(msg));
        } else {
            ctx.close();
            channelsHolder.unsubscribe(path, this);
        }
    }

    @SneakyThrows
    public void sendMessage(PairEntity pairEntity) {
        if (ctx.channel().isActive()) {
            log.debug("Sending message with pipeline {}", ctx.pipeline());
            ctx.writeAndFlush(new WrappedNettyFrame(new TextWebSocketFrame(objectMapper.writeValueAsString(pairEntity)), pairEntity.generationTime()));
        } else {
            ctx.close();
            channelsHolder.unsubscribe(path, this);
        }
    }

    private void removeNonWSHandlers(ChannelHandlerContext ctx) {
        ChannelPipeline pipeline = ctx.pipeline();
        pipeline.remove(NettyPipeline.ReactiveBridge);
        if (pipeline.get(NettyPipeline.HttpTrafficHandler) != null) {
            pipeline.remove(NettyPipeline.HttpTrafficHandler);
        }
        if (pipeline.get(NettyPipeline.AccessLogHandler) != null) {
            pipeline.remove(NettyPipeline.AccessLogHandler);
        }
        if (pipeline.get(NettyPipeline.HttpMetricsHandler) != null) {
            pipeline.remove(NettyPipeline.HttpMetricsHandler);
        }
    }

    private void addCompressionHandler(ChannelHandlerContext ctx, HttpRequest httpRequest) {
        if (ALLOW_WS_COMPRESSION) {
            ChannelPipeline pipeline = ctx.pipeline();
            if (pipeline.get(NettyPipeline.CompressionHandler) != null) {
                pipeline.remove(NettyPipeline.CompressionHandler);
            }

            WebSocketServerCompressionHandler wsServerCompressionHandler =
                    new WebSocketServerCompressionHandler();
            try {
                wsServerCompressionHandler.channelRead(ctx.channel().pipeline().context(NAME), httpRequest);
                pipeline.addBefore(NAME, NettyPipeline.WsCompressionHandler, wsServerCompressionHandler);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private void addTimeoutHandlers(ChannelHandlerContext ctx) {
        ChannelPipeline pipeline = ctx.pipeline();
        pipeline.addBefore(NAME, WriteTimeoutHandler.NAME, new WriteTimeoutHandler(3, TimeUnit.SECONDS));
        pipeline.addBefore(NAME, IdleStateHandler.NAME, new IdleStateHandler(0, 0, 10));
    }

    private void handleHandshake(ChannelHandlerContext ctx, HttpRequest req, Long receivedTime) {
        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        WebSocketServerHandshaker handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            HttpRequest fullRequest = new DefaultFullHttpRequest(req.protocolVersion(), req.method(), req.uri());
            fullRequest.headers().set(req.headers());
            handshaker.handshake(ctx.channel(), fullRequest)
                    .addListener(f -> {
                        ctx.channel().read();
                    });
        }
    }

    private String getWebSocketURL(HttpRequest request) {
        return "ws://" + request.headers().get(HttpHeaderNames.HOST) + request.uri();
    }
}
