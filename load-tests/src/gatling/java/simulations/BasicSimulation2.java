package simulations;

import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import static io.gatling.javaapi.core.CoreDsl.constantUsersPerSec;
import static io.gatling.javaapi.core.CoreDsl.scenario;
import static io.gatling.javaapi.http.HttpDsl.http;

public class BasicSimulation2 extends Simulation {

  HttpProtocolBuilder httpProtocol;

  {
    String gatlingBaseUrl = System.getenv("GATLING_BASE_URL");
    System.out.println(gatlingBaseUrl);

    httpProtocol = http
            .baseUrl(gatlingBaseUrl)
            .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
            .doNotTrackHeader("1")
            .acceptLanguageHeader("en-US,en;q=0.5")
            .acceptEncodingHeader("gzip, deflate")
            .userAgentHeader("Gatling2");
  }

  ScenarioBuilder scn = scenario("Scenario 2")
          .exec(http("Get with foo param 2").get("/path/foo"));

  {
    setUp(scn.injectOpen(constantUsersPerSec(3).during(10)).protocols(httpProtocol));
  }
}
