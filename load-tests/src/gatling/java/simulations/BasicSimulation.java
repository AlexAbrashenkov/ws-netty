package simulations;

import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.http;
import static io.gatling.javaapi.http.HttpDsl.status;

public class BasicSimulation extends Simulation {

  HttpProtocolBuilder httpProtocol;

  {
    String gatlingBaseUrl = System.getenv("GATLING_BASE_URL");
    System.out.println(gatlingBaseUrl);

    httpProtocol = http
            .baseUrl(gatlingBaseUrl)
            .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
            .doNotTrackHeader("1")
            .acceptLanguageHeader("en-US,en;q=0.5")
            .acceptEncodingHeader("gzip, deflate")
            .userAgentHeader("Gatling2");
  }

  ScenarioBuilder scn = scenario("Scenario 1")
          .exec(http("Get with test param 1").get("/path/test").check(status().is(200)));

  {
    setUp(scn.injectOpen(constantUsersPerSec(3).during(10)).protocols(httpProtocol))
    .assertions(
            global().successfulRequests().percent().gt(95.0),
            global().responseTime().max().lt(100),
            global().responseTime().mean().lt(50)
            );
  }
}
