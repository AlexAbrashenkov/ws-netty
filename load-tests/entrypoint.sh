#!/bin/bash

rm -rf results
mkdir -p results/reports

shopt -s globstar nullglob
for f in $(grep -rnwl . -e "extends *Simulation") ; do
  echo "Starting simulation $f"
  # execute test
  simulation_name=$(echo "$f" | sed -r 's/(\.\/|user-files\/|\.java)//g' | tr / .)
  bin/gatling.sh -nr -sf "user-files" -s "$simulation_name"

  # save simulation log
  path=$(ls -t results | head -n 1)
  report_name=$(echo "$simulation_name" | sed 's/\(.\)[^\.]*\./\1\./g').log
  mv "results/$path/simulation.log" "results/reports/$report_name"

  rm -rf "results/$path"
done

echo "Generation reports for all tests..."
bin/gatling.sh -ro reports | tee results/reports/summary.log

assertions_start_line=$(awk '/Please open/{ print ++NR; exit }' results/reports/summary.log)
declare -a failed_assertions
while read line; do
    result=${line##*:}
    if [[ ${result} =~ \s*false\s* ]]; then
      failed_assertions+=("$line")
    fi
done < <(awk "NR >= $assertions_start_line" results/reports/summary.log)

if [[ ${#failed_assertions[@]} -gt 0 ]]; then
  {
    echo "Failed assertions:"
    printf "%s\n" "${failed_assertions[@]}"
    echo "================================================================================"
  } >> results/reports/summary_cut.log
fi

awk '$1=="================================================================================"{c++;next} c==1' \
results/reports/summary.log >> results/reports/summary_cut.log